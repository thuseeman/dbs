package com.thusee.dbsassessment

/**
 * Created by rthusee on 14/6/20
 */
object TestUtils {

    fun setProperty(instance: Any, name: String, param: Any?) {

        val field = instance.javaClass.getDeclaredField(name)
        field.isAccessible = true
        field.set(instance, param)
    }

    fun <T> getProperty(instance: Any, name: String): T {

        val field = instance.javaClass.getDeclaredField(name)
        field.isAccessible = true
        return field.get(instance) as T
    }

    fun invokeMethod(instance: Any, methodName: String, vararg arguments: Any): Any? {

        val clazz = arrayOfNulls<Class<*>>(arguments.size)
        arguments.forEachIndexed { index, it ->
            clazz[index] = it::class.java
        }

        val method = instance.javaClass.getDeclaredMethod(methodName, *clazz)
        method.isAccessible = true
        return method.invoke(instance, *arguments)
    }
}