package com.thusee.dbsassessment.views.article

import androidx.lifecycle.MutableLiveData
import com.thusee.dbsassessment.InstantTaskExecutorRule
import com.thusee.dbsassessment.TestUtils
import com.thusee.dbsassessment.data.ArticleResponse
import com.thusee.dbsassessment.usecase.UiEvent
import io.mockk.*
import io.reactivex.disposables.Disposable
import junit.framework.Assert.assertEquals
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

/**
 * Created by rthusee on 14/6/20
 */
internal class ArticleObserverTest: Spek({

    val rule = InstantTaskExecutorRule()

    lateinit var mockArticleList: MutableLiveData<List<ArticleResponse>>
    lateinit var mockViewState: MutableLiveData<UiEvent>

    lateinit var instance: ArticleObserver

    beforeEachTest {
        rule.start()

        mockArticleList = mockk(relaxed = true)
        mockViewState = mockk(relaxed = true)

        instance = ArticleObserver(mockArticleList, mockViewState)
    }

    afterEachTest {
        rule.stop()
        unmockkAll()
    }

    describe("dispose()") {
        context("when disposable is null") {
            it("shouldn't invoke the dispose on disposable") {
                val mockDisposable = mockk<Disposable>(relaxed = true)

                instance.dispose()
                verify(exactly = 0) { mockDisposable.dispose() }
            }
        }

        context("when disposable is not null") {
            it("should invoke the dispose() on disposable") {

                val mockDisposable = mockk<Disposable>(relaxed = true)
                TestUtils.setProperty(instance, "disposable", mockDisposable)
                every { mockDisposable.dispose() } just Runs

                instance.dispose()
                verify { mockDisposable.dispose() }
            }
        }
    }

    describe("onSubscribe()") {
        context("Given disposable") {
            it("Then set local disposable, and set viewState value to ArticleUiEvent#ShowProgressBar") {

                val mockDisposable = mockk<Disposable>(relaxed = true)

                val slot = CapturingSlot<UiEvent>()

                instance.onSubscribe(mockDisposable)

                val result: Disposable = TestUtils.getProperty(instance, "disposable")
                assertEquals(mockDisposable, result)

                verify { mockViewState.value = capture(slot) }
                assertEquals(UiEvent.ShowProgressBar::class, slot.captured::class)
            }
        }
    }

    describe("onSuccess()") {
        it("Given list, update the liveData and set viewState value to ArticleUiEvent#HideProgressBar") {
            val mockList = listOf<ArticleResponse>()

            val slot = CapturingSlot<UiEvent>()

            instance.onSuccess(mockList)

            verify { mockViewState.value = capture(slot) }
            assertEquals(UiEvent.HideProgressBar::class, slot.captured::class)

            verify { mockArticleList.value = mockList }
        }
    }

    describe("onComplete") {
        it("set viewState value to ArticleUiEvent#HideProgressBar") {

            val slot = CapturingSlot<UiEvent>()
            instance.onComplete()

            verify { mockViewState.value = capture(slot) }
            assertEquals(UiEvent.HideProgressBar::class, slot.captured::class)

            verify { mockArticleList wasNot Called }
        }
    }

    describe("onError()") {
        it("Given exception") {
            val exception = Throwable()
            val slotHide = CapturingSlot<UiEvent>()
            val slot = CapturingSlot<UiEvent>()

            instance.onError(exception)

            verify { mockViewState.value = capture(slotHide) }
            assertEquals(UiEvent.HideProgressBar::class, slotHide.captured::class)

            verify { mockViewState.postValue(capture(slot)) }
            assertEquals(UiEvent.ErrorHandle::class, slot.captured::class)

            verify { mockArticleList wasNot Called }
        }
    }

})