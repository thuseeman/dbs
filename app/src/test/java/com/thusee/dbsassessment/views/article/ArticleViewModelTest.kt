package com.thusee.dbsassessment.views.article

import com.thusee.dbsassessment.InstantTaskExecutorRule
import com.thusee.dbsassessment.RxImmediateSchedulerRule
import com.thusee.dbsassessment.data.ApiService
import io.mockk.every
import io.mockk.mockk
import io.mockk.unmockkAll
import io.mockk.verify
import io.reactivex.Maybe
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

/**
 * Created by rthusee on 14/6/20
 */
internal class ArticleViewModelTest: Spek({

    val rule = InstantTaskExecutorRule()
    val rule2 = RxImmediateSchedulerRule()

    lateinit var mockApiService : ApiService

    lateinit var instance: ArticleViewModel

    beforeEachTest {
        rule.start()
        rule2.run()

        mockApiService = mockk(relaxed = true)

        instance = ArticleViewModel(mockApiService)
    }

    afterEachTest {
        unmockkAll()
        rule.stop()
        rule2.reset()
    }

    describe("fetchRemoteData()") {
        context("Given, ApiService fetchRemoteArticle() call") {
            it("fetchRemoteArticle() api will call") {
                every { mockApiService.fetchRemoteArticle() } returns Maybe.just(listOf())

                instance.fetchRemoteData()

                verify { mockApiService.fetchRemoteArticle() }
            }
        }
    }

})