package com.thusee.dbsassessment.views.details

import com.thusee.dbsassessment.InstantTaskExecutorRule
import com.thusee.dbsassessment.RxImmediateSchedulerRule
import com.thusee.dbsassessment.data.ApiService
import com.thusee.dbsassessment.data.DetailsResponse
import io.mockk.every
import io.mockk.mockk
import io.mockk.unmockkAll
import io.mockk.verify
import io.reactivex.Maybe
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

/**
 * Created by rthusee on 14/6/20
 */
internal class DetailsViewModelTest: Spek({

    val rule = InstantTaskExecutorRule()
    val rule2 = RxImmediateSchedulerRule()

    lateinit var mockApiService : ApiService

    lateinit var instance: DetailsViewModel

    beforeEachTest {
        rule.start()
        rule2.run()

        mockApiService = mockk(relaxed = true)

        instance = DetailsViewModel(mockApiService)
    }

    afterEachTest {
        unmockkAll()
        rule.stop()
        rule2.reset()
    }

    describe("fetchRemoteData()") {
        context("Given, ApiService fetchArticleDetails() call") {
            it("fetchArticleDetails() api will call") {
                every { mockApiService.fetchArticleDetails(1) } returns Maybe.just(DetailsResponse(id = 1, text = "Test"))

                instance.fetchDetailsData(id = 1)

                verify { mockApiService.fetchArticleDetails(any()) }
            }
        }
    }

})