package com.thusee.dbsassessment.views.article

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.thusee.dbsassessment.data.ApiService
import com.thusee.dbsassessment.data.ArticleResponse
import com.thusee.dbsassessment.usecase.UiEvent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.koin.core.KoinComponent

/**
 * Created by rthusee on 14/6/20
 */
class ArticleViewModel (private val apiService: ApiService) : ViewModel(), KoinComponent {

    val articleListLiveData = MutableLiveData<List<ArticleResponse>>()
    val articleViewState = MutableLiveData<UiEvent>()

    private var articleObserver: ArticleObserver? = null

    fun fetchRemoteData() {
        articleObserver = ArticleObserver(articleListLiveData, articleViewState)

        apiService.fetchRemoteArticle()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(articleObserver!!)
    }

    override fun onCleared() {
        super.onCleared()
        articleObserver?.dispose()
    }

}