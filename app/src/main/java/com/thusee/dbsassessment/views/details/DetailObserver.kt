package com.thusee.dbsassessment.views.details

import androidx.lifecycle.MutableLiveData
import com.thusee.dbsassessment.data.DetailsResponse
import com.thusee.dbsassessment.usecase.UiEvent
import io.reactivex.MaybeObserver
import io.reactivex.disposables.Disposable
import timber.log.Timber

/**
 * Created by rthusee on 14/6/20
 */
class DetailObserver(
    private val articleDetails: MutableLiveData<DetailsResponse>,
    private val viewState: MutableLiveData<UiEvent>
) : MaybeObserver<DetailsResponse> {

    private var disposable: Disposable? = null

    fun dispose() {
        disposable?.dispose()
        disposable = null
    }

    override fun onSuccess(details: DetailsResponse) {
        articleDetails.value = details
        viewState.value = UiEvent.HideProgressBar
    }

    override fun onComplete() {
        viewState.value = UiEvent.HideProgressBar
    }

    override fun onSubscribe(d: Disposable) {
        disposable = d
        viewState.value = UiEvent.ShowProgressBar
    }

    override fun onError(e: Throwable) {
        viewState.value = UiEvent.HideProgressBar
        viewState.postValue(UiEvent.ErrorHandle(e))
        Timber.d("$e")
    }

}