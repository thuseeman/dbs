package com.thusee.dbsassessment.views.details

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.thusee.dbsassessment.data.ApiService
import com.thusee.dbsassessment.data.DetailsResponse
import com.thusee.dbsassessment.usecase.UiEvent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.koin.core.KoinComponent

/**
 * Created by rthusee on 14/6/20
 */
class DetailsViewModel (private val apiService: ApiService): ViewModel(), KoinComponent {

    val detailsLiveData = MutableLiveData<DetailsResponse>()
    val articleViewState = MutableLiveData<UiEvent>()

    private var detailsObserver: DetailObserver? = null

    fun fetchDetailsData(id: Int) {
        detailsObserver = DetailObserver(detailsLiveData, articleViewState)

        apiService.fetchArticleDetails(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(detailsObserver!!)
    }

    override fun onCleared() {
        super.onCleared()
        detailsObserver?.dispose()
    }

}