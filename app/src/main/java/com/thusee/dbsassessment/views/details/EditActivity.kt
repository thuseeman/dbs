package com.thusee.dbsassessment.views.details

import android.os.Bundle
import androidx.appcompat.app.ActionBar
import com.thusee.dbsassessment.R
import com.thusee.dbsassessment.views.base.BaseActivity
import com.thusee.dbsassessment.views.details.DetailsActivity.Companion.ARTICLE_DESC
import com.thusee.dbsassessment.views.details.DetailsActivity.Companion.ARTICLE_TITLE
import kotlinx.android.synthetic.main.edit_activity.*
import kotlinx.android.synthetic.main.toolbar.*

/**
 * Created by rthusee on 14/6/20
 */
class EditActivity : BaseActivity() {

    private var text: String? = ""
    private var title: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.edit_activity)

        setupToolbar()
        text = intent.getStringExtra(ARTICLE_DESC)
        title = intent.getStringExtra(ARTICLE_TITLE)

        editDesc.setText(text)

        saveButton.setOnClickListener {
            onBackPressed()
        }
    }

    private fun setupToolbar() {

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setCustomView(R.layout.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        toolbarTitle.text = title

        toolbarRightButton.text = getString(R.string.cancel)
        toolbarRightButton.setOnClickListener {
            onBackPressed()
        }
        toolbarBack.setOnClickListener {
            onBackPressed()
        }
    }

}