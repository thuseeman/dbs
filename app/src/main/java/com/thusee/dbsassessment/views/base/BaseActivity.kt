package com.thusee.dbsassessment.views.base

import androidx.appcompat.app.AppCompatActivity
import com.thusee.dbsassessment.usecase.PopupDialog

/**
 * Created by rthusee on 14/6/20
 */
abstract class BaseActivity : AppCompatActivity() {

    fun showPopupDialog(title: String, msg: String) {
        PopupDialog.newInstance(title, msg).show(supportFragmentManager,
            "popup_dialog")
    }
}