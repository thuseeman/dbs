package com.thusee.dbsassessment.views.article

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.thusee.dbsassessment.R
import com.thusee.dbsassessment.data.ArticleResponse
import com.thusee.dbsassessment.event.AdapterItemClick
import com.thusee.dbsassessment.usecase.UiEvent
import com.thusee.dbsassessment.util.handleError
import com.thusee.dbsassessment.views.base.BaseActivity
import com.thusee.dbsassessment.views.details.DetailsActivity
import com.thusee.dbsassessment.views.details.DetailsActivity.Companion.ARTICLE_DATA_OBJECT
import kotlinx.android.synthetic.main.activity_article.*
import org.koin.android.viewmodel.ext.android.viewModel

class ArticleActivity : BaseActivity(), AdapterItemClick {

    private val articleViewModel: ArticleViewModel by viewModel()
    private lateinit var adapter: ArticleAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_article)

        initView()
        updateAdapter()
        setupViewModel()

        articleViewModel.fetchRemoteData()
    }

    private fun initView() {
        articleRecycler.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)

        adapter = ArticleAdapter(this, this)
        articleRecycler.adapter = adapter
        val dividerItemDecoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        dividerItemDecoration.setDrawable(
            ContextCompat.getDrawable(this, R.drawable.devider_article_item)!!
        )
        articleRecycler.addItemDecoration(dividerItemDecoration)
    }

    private fun articleRender(uiEvent: UiEvent) {
        when (uiEvent) {
            is UiEvent.ShowProgressBar -> rowLoadingAnim.visibility = View.VISIBLE
            is UiEvent.HideProgressBar -> rowLoadingAnim.visibility = View.GONE
            is UiEvent.ErrorHandle -> {
                showPopupDialog(getString(R.string.error), handleError(uiEvent.e))
            }
        }
    }

    private fun setupViewModel() {
        articleViewModel.articleViewState.observe(this, Observer { it ->
            it?.let {
                articleRender(it)
            }
        })
    }

    private fun updateAdapter() {
        articleViewModel.articleListLiveData.observe(this, Observer {
            it?.let {
                adapter.addAll(it)
            }
        })
    }

    override fun onItemClick(article: ArticleResponse) {
       val intent = Intent(this, DetailsActivity::class.java)
        intent.putExtra(ARTICLE_DATA_OBJECT, article)
        startActivity(intent)
    }

}