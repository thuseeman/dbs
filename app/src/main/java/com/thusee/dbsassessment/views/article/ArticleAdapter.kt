package com.thusee.dbsassessment.views.article

import android.content.Context
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.thusee.dbsassessment.R
import com.thusee.dbsassessment.data.ArticleResponse
import com.thusee.dbsassessment.event.AdapterItemClick
import com.thusee.dbsassessment.util.loadImage
import java.util.concurrent.TimeUnit

/**
 * Created by rthusee on 14/6/20
 */
class ArticleAdapter (
    private val context: Context,
    private val adapterActionListener: AdapterItemClick
) : RecyclerView.Adapter<ArticleAdapter.ArticleViewHolder>() {

    private var articles = arrayListOf<ArticleResponse>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleViewHolder {
        return ArticleViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.article_item, parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return articles.size
    }

    fun addAll(tempList: List<ArticleResponse>) {
        articles.clear()
        articles.addAll(tempList)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ArticleViewHolder, position: Int) {
        val article = articles[position]
        holder.title.text = article.title
        holder.shortDesc.text = article.shortDescription

        holder.date.text = getTimeElapsed(context, article.lastUpdate.toLong()*1000)

        holder.imageView.loadImage(article.avatar)

        holder.itemView.setOnClickListener {
            adapterActionListener.onItemClick(article)
        }

    }

    class ArticleViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val imageView: AppCompatImageView = view.findViewById(R.id.articleImage)
        val title: AppCompatTextView = view.findViewById(R.id.articleTitle)
        val date: AppCompatTextView = view.findViewById(R.id.articleDate)
        val shortDesc: AppCompatTextView = view.findViewById(R.id.articleShortDesc)
    }

    private fun getTimeElapsed(context: Context, timeAgo: Long): String {
        var dateAgo = DateUtils.getRelativeTimeSpanString(
            timeAgo,
            System.currentTimeMillis(),
            DateUtils.HOUR_IN_MILLIS
        )

        val diffInMillisec = System.currentTimeMillis() - timeAgo
        val diffInHours = TimeUnit.MILLISECONDS.toHours(diffInMillisec)

        if (diffInHours < 1) {
            dateAgo = context.getString(R.string.date_now)
        }
        return dateAgo.toString()
    }
}