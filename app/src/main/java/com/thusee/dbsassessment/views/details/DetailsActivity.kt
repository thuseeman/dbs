package com.thusee.dbsassessment.views.details

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.ActionBar
import androidx.lifecycle.Observer
import com.thusee.dbsassessment.R
import com.thusee.dbsassessment.data.ArticleResponse
import com.thusee.dbsassessment.util.handleError
import com.thusee.dbsassessment.usecase.UiEvent
import com.thusee.dbsassessment.util.loadImage
import com.thusee.dbsassessment.views.base.BaseActivity
import kotlinx.android.synthetic.main.activity_article.*
import kotlinx.android.synthetic.main.activity_details.*
import kotlinx.android.synthetic.main.toolbar.*
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * Created by rthusee on 14/6/20
 */
class DetailsActivity : BaseActivity() {

    private val articleDetailsViewModel: DetailsViewModel by viewModel()

    private lateinit var article: ArticleResponse

    companion object {
        const val ARTICLE_DATA_OBJECT = "com.thusee.dbsapp.view.articledetails.ARTICLE_DATA_OBJECT"
        const val ARTICLE_DESC = "com.thusee.dbsapp.view.articledetails.ARTICLE_TEXT"
        const val ARTICLE_TITLE = "com.thusee.dbsapp.view.articledetails.ARTICLE_TITLE"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        setupToolbar()

        article = intent.getParcelableExtra(ARTICLE_DATA_OBJECT)!!
        article.let {
            articleDetailsViewModel.fetchDetailsData(article.id)
        }
        setupViewModel()
        updateUi()

    }

    private fun setupToolbar() {

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setCustomView(R.layout.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        toolbarTitle.text = article.title

        toolbarRightButton.text = getString(R.string.edit)
        toolbarRightButton.setOnClickListener {
            val intent = Intent(this, EditActivity::class.java)
            intent.putExtra(ARTICLE_DESC, detailsDesc.text)
            intent.putExtra(ARTICLE_TITLE, article.title)
            startActivity(intent)
        }
        toolbarBack.setOnClickListener {
            onBackPressed()
        }
    }

    private fun detailsRender(uiEvent: UiEvent) {
        when (uiEvent) {
            is UiEvent.ShowProgressBar -> rowLoadingAnim.visibility = View.VISIBLE
            is UiEvent.HideProgressBar -> rowLoadingAnim.visibility = View.GONE
            is UiEvent.ErrorHandle -> {
                showPopupDialog(getString(R.string.error), handleError(uiEvent.e))
            }
        }
    }

    private fun setupViewModel() {
        articleDetailsViewModel.articleViewState.observe(this, Observer { it ->
            it?.let {
                detailsRender(it)
            }
        })
    }

    private fun updateUi() {

        detailsImage.loadImage(article.avatar)

        articleDetailsViewModel.detailsLiveData.observe(this, Observer { it ->
            it?.let {
                detailsDesc.text = it.text
            }
        })
    }

}