package com.thusee.dbsassessment.util

import android.content.Context
import androidx.appcompat.widget.AppCompatImageView
import com.bumptech.glide.Glide
import com.thusee.dbsassessment.R
import retrofit2.HttpException
import java.net.UnknownHostException

/**
 * Created by rthusee on 14/6/20
 */

fun Context.handleError(e: Throwable): String {
    if (e is HttpException) {
        val body = e.response()?.errorBody()
        val errorMessage = body?.string()

        if (errorMessage!!.isNotEmpty()) {
            // Need to throw the error msg according to the API Response then,
            // I only know the 429 code else pass the errorMessage from the API
            return when (e.code()) {
                429 -> {
                    getString(R.string.too_many_request)
                }
                else -> {
                    errorMessage
                }
            }
        }
        return getString(R.string.failed_error)
    } else if (e is UnknownHostException) {
        return getString(R.string.network_not_available)
    }

    return getString(R.string.failed_error)
}

fun AppCompatImageView.loadImage(url: String) {
    Glide.with(this)
        .load(url)
        .placeholder(R.mipmap.ic_launcher)
        .circleCrop()
        .into(this)
}
