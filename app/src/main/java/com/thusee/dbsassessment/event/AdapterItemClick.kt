package com.thusee.dbsassessment.event

import com.thusee.dbsassessment.data.ArticleResponse

/**
 * Created by rthusee on 14/6/20
 */
interface AdapterItemClick {
    fun onItemClick(article: ArticleResponse)
}