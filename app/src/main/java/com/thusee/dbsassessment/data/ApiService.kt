package com.thusee.dbsassessment.data

import io.reactivex.Maybe
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by rthusee on 14/6/20
 */
interface ApiService {

    @GET("/article")
    fun fetchRemoteArticle(): Maybe<List<ArticleResponse>>

    @GET("/article/{id}")
    fun fetchArticleDetails(@Path("id") id: Int): Maybe<DetailsResponse>
}