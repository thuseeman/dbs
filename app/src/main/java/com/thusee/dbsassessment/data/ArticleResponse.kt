package com.thusee.dbsassessment.data

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

/**
 * Created by rthusee on 14/6/20
 */
@Parcelize
data class ArticleResponse(
    @field:Json(name = "id") val id: Int = 0,
    @field:Json(name = "title") val title: String? = "",
    @field:Json(name = "last_update") val lastUpdate: Int = 0,
    @field:Json(name = "short_description") val shortDescription: String = "",
    @field:Json(name = "avatar") val avatar: String = ""
) : Parcelable