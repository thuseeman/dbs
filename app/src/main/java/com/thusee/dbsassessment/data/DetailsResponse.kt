package com.thusee.dbsassessment.data

import com.squareup.moshi.Json

/**
 * Created by rthusee on 14/6/20
 */
data class DetailsResponse(
    @field:Json(name = "id") val id: Int = 0,
    @field:Json(name = "text") var text: String = ""
)