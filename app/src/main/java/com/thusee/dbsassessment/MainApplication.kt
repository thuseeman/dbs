package com.thusee.dbsassessment

import android.app.Application
import com.thusee.dbsassessment.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

/**
 * Created by rthusee on 14/6/20
 */
class MainApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())

        startKoin {
            androidContext(this@MainApplication)
            modules(appModule)
        }
    }
}