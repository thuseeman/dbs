package com.thusee.dbsassessment.usecase

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.thusee.dbsassessment.R

/**
 * Created by rthusee on 14/6/20
 */
class PopupDialog : DialogFragment() {

    private var titleText: String? = ""
    private var messages: String? = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val view = inflater.inflate(R.layout.popup_dialog, container)
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog?.window?.setBackgroundDrawableResource(android.R.color.transparent)

        view.findViewById<TextView>(R.id.dialogTitle).text = titleText
        view.findViewById<TextView>(R.id.dialogMsg).text = messages
        isCancelable = false

        view.findViewById<Button>(R.id.dialogOkButton).setOnClickListener {
            dismiss()
        }

        return view
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
    }

    companion object {
        fun newInstance(
            title: String,
            msg: String
        ): PopupDialog {
            val dialog = PopupDialog()
            dialog.titleText = title
            dialog.messages = msg
            return dialog
        }
    }
}