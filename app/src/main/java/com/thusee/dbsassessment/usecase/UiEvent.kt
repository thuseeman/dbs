package com.thusee.dbsassessment.usecase

/**
 * Created by rthusee on 14/6/20
 */
sealed class UiEvent {
    object ShowProgressBar: UiEvent()
    object HideProgressBar: UiEvent()
    data class ErrorHandle(val e: Throwable) : UiEvent()
}