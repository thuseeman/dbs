package com.thusee.dbsassessment.di

import com.thusee.dbsassessment.BuildConfig
import com.thusee.dbsassessment.data.ApiService
import com.thusee.dbsassessment.views.article.ArticleViewModel
import com.thusee.dbsassessment.views.details.DetailsViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

/**
 * Created by rthusee on 14/6/20
 */

val appModule = module {

    viewModel { ArticleViewModel(get()) }
    viewModel { DetailsViewModel(get()) }

    factory<ApiService> {
        Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build().create(ApiService::class.java)
    }

}